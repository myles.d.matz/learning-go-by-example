/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

// cleanCmd represents the clean command
var cleanCmd = &cobra.Command{
	Use:   "clean",
	Short: "Cleans out .png images",
	Long:  `Searches current directory and removes all *.png files`,
	Run: func(cmd *cobra.Command, args []string) {
		current_dir, err := os.Getwd()
		if err != nil {
			fmt.Println(err)
		}

		files, err := ioutil.ReadDir(current_dir)
		if err != nil {
			fmt.Println(err)
		}

		for _, file := range files {
			if strings.Contains(file.Name(), ".png") {
				os.Remove(file.Name())
				fmt.Println("Succesfully removed, " + file.Name())
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(cleanCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// cleanCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// cleanCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
