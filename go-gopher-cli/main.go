/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import "learning-go-by-example/go-gopher-cli/cmd"

func main() {
	cmd.Execute()
}
